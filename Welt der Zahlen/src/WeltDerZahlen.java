/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    byte anzahlPlaneten = 8;
    
    // Anzahl der Sterne in unserer Milchstra�e
    long anzahlSterne = 2000000000; 
    
    // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = 3800000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    short   alterTage = 6953; 
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int gewichtKilogramm =   190000; 
    
    // Schreiben Sie auf, wie viele km² das gr��te Land der Erde hat?
    int   flaecheGroessteLand = 17100000;
    	
    
    // Wie gro� ist das kleinste Land der Erde?
    
    double   flaecheKleinsteLand = 0.49;
    
    
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzahl der Planeten in unserem Sonnensystem: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne in unserer Milchstra�e: " + anzahlSterne);
    
    System.out.println("Wie viele Einwohner hat Berlin?: " + bewohnerBerlin);
    
    System.out.println("Wie alt bist du?  Wie viele Tage sind das?: " + alterTage);
    
    System.out.println("Wie viel wiegt das schwerste Tier der Welt? Schreiben Sie das Gewicht in Kilogramm auf!: " + gewichtKilogramm);
    
    System.out.println("Schreiben Sie auf, wie viele km² das gr��te Land der Erde hat?: " + flaecheGroessteLand);
    
    System.out.println("Wie gro� ist das kleinste Land der Erde?: " + flaecheKleinsteLand);
    
    
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

