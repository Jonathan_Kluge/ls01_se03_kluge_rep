﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag;
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;

       System.out.print("Zu zahlender Betrag (EURO): ");
       zuZahlenderBetrag = tastatur.nextDouble();

       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 00.00;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   System.out.printf("Noch zu zahlen: %.2f Euro%n " ,  (zuZahlenderBetrag - eingezahlterGesamtbetrag));
    	   System.out.print("Eingabe (mind. 0.50 Euro, höchstens 2.00 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)  
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rückgabebetrag > 00.00)
       {
    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO%n",rückgabebetrag);
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.00) // 2.00 EURO-Münzen
           {
        	  System.out.println("2.00 EURO");
	          rückgabebetrag -= 2.00; //Operator= - 
           }
           while(rückgabebetrag >= 1.00) // 1.00 EURO-Münzen
           {
        	  System.out.println("1.00 EURO");
	          rückgabebetrag -= 1.00;
           }
           while(rückgabebetrag >= 0.50) // 0.50 EURO-Münzen
           {
        	  System.out.println("0.50 EURO");
        	  
	          rückgabebetrag -= 0.50;
           }
           while(rückgabebetrag >= 0.20) // 0.20 EURO-Münzen
           {
        	  System.out.println("0.20 EURO");
 	          rückgabebetrag -= 0.20;
           }
           while(rückgabebetrag >= 0.10) // 0.10 EURO-Münzen
           {
        	  System.out.println("0.10 EURO");
	          rückgabebetrag -= 0.10;
           }
           while(rückgabebetrag >= 0.05)// 0.05 EURO-Münzen
           {
        	  System.out.println("0.05 EURO");
 	          rückgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
}